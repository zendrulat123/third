module github.com/zendrulat

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/rs/cors v1.7.0 // indirect
)
