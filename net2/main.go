package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/rs/cors"
)

func main() {

	mux := http.NewServeMux()
	//handlers
	run()
	mux.HandleFunc("/find", Find)
	handler := cors.Default().Handler(mux)
	log.Fatal(http.ListenAndServe(":8082", handler))

}

//searching db.
func Find(w http.ResponseWriter, r *http.Request) {
	//user for data.
	type User struct {
		Id       int    `json:"id,number"`
		Username string `json:"username,omitempty"`
	}

	url := fmt.Sprintf("http://localhost:8084/search")

	// Build the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}
	// For control over HTTP client headers,
	// redirect policy, and other settings,
	// create a Client
	// A Client is an HTTP client
	client := &http.Client{}

	// Send the request via a client
	// Do sends an HTTP request and
	// returns an HTTP response
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return
	}

	// Callers should close resp.Body
	// when done reading from it
	// Defer the closing of the body
	defer resp.Body.Close()
	var U User
	if err := json.NewDecoder(resp.Body).Decode(&U); err != nil {
		log.Println(err)
	}
	var i int
	i++
	fmt.Println("Thank you the User is ", U, i)
	return
}

func run() {
	//user for data.
	type User struct {
		Id       int    `json:"id,number"`
		Username string `json:"username,omitempty"`
	}

	url := fmt.Sprintf("http://localhost:8084/search")

	// Build the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}
	// For control over HTTP client headers,
	// redirect policy, and other settings,
	// create a Client
	// A Client is an HTTP client
	client := &http.Client{}

	// Send the request via a client
	// Do sends an HTTP request and
	// returns an HTTP response
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return
	}

	// Callers should close resp.Body
	// when done reading from it
	// Defer the closing of the body
	defer resp.Body.Close()
	var U User
	if err := json.NewDecoder(resp.Body).Decode(&U); err != nil {
		log.Println(err)
	}
	var i int
	i++
	fmt.Println("Thank you the User is ", U, i)
	return
}
