package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	Handlers "github.com/third/Headers"
	"github.com/rs/cors"
)

func main() {

	mux := http.NewServeMux()
	//handlers
	mux.HandleFunc("/search", Search)
	handler := cors.Default().Handler(mux)
	log.Fatal(http.ListenAndServe(":8084", handler))

}

//searching db.
func Search(w http.ResponseWriter, r *http.Request) {
	//user for data.
	type User struct {
		Id       int    `json:"id,omitempty"`
		Username string `json:"username,omitempty"`
	}

	Handlers.Header(w, r)

	U := User{Id: 3, Username: "jim"}
	//searching rows

	fmt.Println("before marshal ", U)
	json.NewEncoder(w).Encode(U)

	w.Header().Set("Content-Type", "application/json")
	fmt.Println("You asked for ", U)
	return
}
